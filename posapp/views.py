from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from django.shortcuts import render, redirect, HttpResponse,get_object_or_404
from django.views import View
from .models import UserProfile,Product,Category, Cart
from .forms import CreateUserForm, ChangePassword, SigninForm, ProductaddForm
from django.contrib import messages


class HomeView(View):
    def get(self, request):
        products = Product.objects.all()
        cart = Cart.objects.all() 
        qtys = request.session.get('qtys', 1)
        if request.user.is_authenticated:  
            for item in cart:
              item.subtotal = item.product.price * item.quantity 
            return render(request,'posapp/home.html',{'data': products, 'cart': cart, 'qtys': qtys})
        else:
            return redirect('signin')
    
class AddtoCart(View):
    def get(self, request, pk):
        # Get the product object
        product = Product.objects.get(pk=pk)
        # Create a new Cart object with the current user and selected product
        cart_item, created = Cart.objects.get_or_create(user=request.user, product=product)     
        # If the cart item was already created, increase its quantity by 1
        if not created:
            cart_item.quantity += 1
            cart_item.save() 
        return redirect('home')

class SigninView(View):
    def get(self, request):
        form = SigninForm()
        return render(request, 'posapp/signin.html', {'form': form})

    def post(self, request):
        form = SigninForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                request.session['username'] = username
                return redirect('home')
            else:
                return render(request, 'posapp/register.html', {'form': form})
        return render(request, 'posapp/signin.html', {'form': form})

class RegisterView(View):
    def get(self, request):
        form = CreateUserForm()
        return render(request, 'posapp/register.html', {'form': form})

    def post(self, request):
        form = CreateUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            phone = form.cleaned_data.get('phone')
            # role = form.cleaned_data.get('role')
            UserProfile.objects.create(user=user, phone=phone)
            login(request, user)
            msg = 'Registration Completed, Go To Signin Page'
            return render(request, 'posapp/register.html', {'msg': msg})
        return render(request, 'posapp/register.html', {'form': form})

class SignoutView(View):
    def get(self, request):
        if 'username' in request.session:
            del request.session['username']
        logout(request)
        return redirect('signin')

class ChangePasswordView(View):
    def get(self, request):
        form = ChangePassword(request.user)
        return render(request, 'posapp/change_password.html', {'form': form})

    def post(self, request):
        form = ChangePassword(request.user, request.POST)
        if form.is_valid():
            form.save()
            return redirect('home')
        else:
            return render(request, 'posapp/change_password.html', {'form': form})
        
class IndexView(View):
    def get(self, request):
        return render(request,'posapp/index.html')
    
class TablesView(View):
    def get(self, request):
        return render(request,'posapp/tables.html')
    
class ChartsView(View):
    def get(self, request):
        return render(request,'posapp/charts.html')

class AddproductView(View):
    def get(self, request):
        form = ProductaddForm()
        return render(request, 'posapp/addproduct.html', {'form': form})

    def post(self, request):
        form = ProductaddForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('product_list') 
        return render(request, 'posapp/addproduct.html', {'form': form})


def update(request, pk=None):
    if request.method =='POST':
        if pk is not None:
            instance = Product.objects.get(pk=pk)  # Use Details instead of Contact
            form = ProductaddForm(request.POST, request.FILES, instance=instance)  
            if form.is_valid():
                form.save()
                messages.success(request, 'Updated Successfully.')
                return redirect('home')  # Redirect to the home view after updating
            else:
                return render(request, 'posapp/addproduct.html', {'form': form})
        else:
            return redirect('home')  # Redirect to the team view if pk is None
    else:
        if pk is not None:
            instance = Product.objects.get(pk=pk)
            form = ProductaddForm(instance=instance)
        else:
            form = ProductaddForm()
        return render(request, 'posapp/addproduct.html', {'form': form})

    
def save(request):
    if request.method =='POST':
        form =ProductaddForm(request.POST, request.FILES)  
        if form.is_valid():
            form.save()
            form=ProductaddForm()
            messages.success(request, 'Data saved Successfully.')
            return redirect('addprdt')
        else:
            return render(request,'posapp/addproduct.html',{'form':form})
    else:
         return render(request,'posapp/addproduct.html',{'form':form})

class EditView(View):
    def get(self, request):
        data=Product.objects.all()
        return render(request,'posapp/edit_product.html',{'data':data})
    
# def Delete(request,pk=None):
#     Product.objects.filter(pk=pk).delete()
#     return redirect('edit_product')
# def edit(request,pk=None):
#     instance= get_object_or_404(Product, pk=pk)
#     form = ProductaddForm(instance=instance)
#     return render(request,"posapp/addproduct.html" ,{'form':form,'instance':instance})
   
# def delete(request):
#     Cart.objects.all().delete()
#     return redirect('home')
class ProductDeleteView(View):
    def get(self, request, pk=None):  # Add the method name here
        Product.objects.filter(pk=pk).delete()
        return redirect('edit_product')

class ProductEditView(View):
    def get(self, request, pk=None):
        instance = get_object_or_404(Product, pk=pk)
        form = ProductaddForm(instance=instance)
        return render(request, "posapp/addproduct.html", {'form': form, 'instance': instance})

class CartDeleteView(View):
    def get(self, request):
        Cart.objects.all().delete()
        return redirect('home')





