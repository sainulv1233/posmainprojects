from django.db import models
from django.contrib.auth.models import User
import random

# user auth model
class userForm(models.Model):
    user = models.OneToOneField('auth.user', on_delete=models.CASCADE)
    phone = models.IntegerField()


# user auth model
class userForm(models.Model):
    user = models.OneToOneField('auth.user', on_delete=models.CASCADE)
    phone = models.IntegerField()


# Category model
class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self) -> str:
        return self.name

# Product model
class Product(models.Model):
    name = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    stock = models.PositiveIntegerField(default=0)
    image = models.ImageField(upload_to='profile',default="")
    

    def __str__(self) -> str:
       return f"{self.category} {self.name} on {self.price}"
   
class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    def total_price(self):
        return self.product.price * self.quantity

    def __str__(self):
        return f"{self.user.username}'s Cart: {self.product.name} x {self.quantity}"
   

# Sales model
class Sale(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField()
    total_price = models.DecimalField(max_digits=10, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)
    staff = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.quantity} {self.product} on {self.date}"
    

class Customer(models.Model):
    username = models.OneToOneField(User, on_delete=models.CASCADE)
    phone_number = models.CharField(max_length=15, blank=True, null=True)
    address = models.TextField(blank=True, null=True)
    loyalty_points = models.PositiveIntegerField(default=0)

    def __str__(self) -> str:
        return self.user.username
    
class Order(models.Model):
    sales = models.ManyToManyField(Sale)
    total_amount = models.DecimalField(max_digits=10, decimal_places=2)
    order_date = models.DateTimeField(auto_now_add=True)
    customer = models.ForeignKey(User, on_delete=models.CASCADE)  # Assuming a user can place an order (you might need a Customer model)

    def __str__(self) -> str:
        return f"Order for {self.customer} on {self.order_date}"

# Payment model
class Payment(models.Model):
    sale = models.OneToOneField(Sale, on_delete=models.CASCADE)
    amount_paid = models.DecimalField(max_digits=10, decimal_places=2)
    payment_date = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return f"Payment for {self.sale}"
    


# Report model (Note: This is just a placeholder; you might need to customize it based on your requirements)
class Report(models.Model):
    date = models.DateField()
    total_sales = models.DecimalField(max_digits=10, decimal_places=2)

    def __str__(self) -> str:
        return f"Report for {self.date}"

# You may also need additional models for authentication, such as UserProfile for custom user fields.

# Example User Profile model
class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.IntegerField(null = True)
    role = models.CharField(max_length=50, choices=[('admin', 'Admin'), ('staff', 'Staff'), ('customer', 'Customer')])

    def __str__(self) -> str:
        return f"{self.user.username} - {self.role}"