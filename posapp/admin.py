from django.contrib import admin
from .models import userForm,Product,Category

admin.site.register(userForm)
admin.site.register(Product)
admin.site.register(Category)

