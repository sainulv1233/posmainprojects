from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, PasswordChangeForm
from django import forms
from django.contrib.auth.models import User
from django import forms
from .models import UserProfile, Product, Category
from django.utils.translation import gettext_lazy as _

class SigninForm(AuthenticationForm):
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

class CreateUserForm(UserCreationForm):
    phone = forms.CharField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    # role = forms.ChoiceField(choices=[('admin', 'Admin'), ('staff', 'Staff'), ('customer', 'Customer')], widget=forms.Select(attrs={'class': 'form-control'}))
    username = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
    email = forms.EmailField(widget=forms.EmailInput(attrs={'class': 'form-control'}))
    password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))

    # class Meta(UserCreationForm.Meta):
    #     model = User
    #     fields = ('username','email','password1','password2','first_name', 'phone', 'role')


class ChangePassword(PasswordChangeForm):
    old_password = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password1 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))
    new_password2 = forms.CharField(widget=forms.PasswordInput(attrs={'class': 'form-control'}))

class CategoryChoiceField(forms.ModelChoiceField):
    def to_python(self, value):
        if value == '':
            return None
        try:
            value = int(value)
        except ValueError:
            pass
        return super().to_python(value)

class ProductaddForm(forms.ModelForm):
    name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    category = CategoryChoiceField(
        queryset=Category.objects.all(),
        widget=forms.Select(attrs={'class':'form-control'}),
        required=False,
        empty_label=_("Create new category")
    )
    price = forms.DecimalField(widget=forms.NumberInput(attrs={'class':'form-control'}))
    stock = forms.IntegerField(widget=forms.NumberInput(attrs={'class':'form-control'}))
    image = forms.ImageField(widget=forms.FileInput(attrs={'class':'form-control'}))
    new_category = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}), required=False)

    class Meta:
        model = Product
        fields = ['name', 'category', 'price', 'stock', 'image', 'new_category']
