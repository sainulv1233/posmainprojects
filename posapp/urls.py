from django.urls import path
from .views import SigninView, SignoutView, RegisterView, ChangePasswordView, HomeView,IndexView, ChartsView, AddtoCart, AddproductView,EditView,ProductDeleteView,ProductEditView,CartDeleteView,TablesView
from django.conf import settings
from django.conf.urls.static import static
from .import views

urlpatterns = [
    path('', SigninView.as_view(), name='signin'),
    path('register', RegisterView.as_view(), name='register'),
    path('change_password', ChangePasswordView.as_view(), name='change_password'),
    path('signout', SignoutView.as_view(), name='signout'),
    path('home', HomeView.as_view(), name='home'),
    path('index', IndexView.as_view(), name='index'),
    path('tables', TablesView.as_view(), name='tables'),
    path('charts', ChartsView.as_view(), name='charts'),
    path('addtocart/<int:pk>/', AddtoCart.as_view(), name='addtocart'),
    path('addprdt', AddproductView.as_view(), name='addprdt'),
    path('save',views.save,name='save'),
    path('update/<int:pk>',views.update,name='update'),
    path('edit_product', EditView.as_view(), name='edit_product'),
    path('delete/<int:pk>',ProductDeleteView.as_view(),name='delete'),
    path('edit/<int:pk>',ProductEditView.as_view(),name='edit'),
    path('delete', CartDeleteView.as_view(), name = 'delete'),

]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
